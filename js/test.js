const chai = window.chai
const expect = window.expect

describe('arrayPrototipeEquals', ()=>{
    it("Equals: Dos matrices iguales", ()=>{        
        expect([[1,2,3],[3,2,1],[4,5,6]].equals([[1,2,3],[3,2,1],[4,5,6]])).to.equal(true);
    });
    it("Equals: Mismos datos, diferente orden", ()=>{
        expect([[1,2,3],[2,3,1],[4,5,6]].equals([[1,2,3],[3,2,1],[4,5,6]])).to.equal(false);
    });
    it("Equals: Una posicion extra", ()=>{
        expect([[1,2,3,3],[3,2,1],[4,5,6]].equals([[1,2,3],[3,2,1],[4,5,6]])).to.equal(false);
    });
    it("Equals: Una posicion menos", ()=>{
        expect([[1,2,3],[4,5,6]].equals([[1,2,3],[3,2,1],[4,5,6]])).to.equal(false);
    });
});
describe('vectoresMovimiento', ()=>{
    it("Peon: Primer movimiento blanca", ()=>{                   
        expect(new Peon(1,1).vectores_movimiento([[-1, -1, -1, -1, -1],[-1, 1, 1, 1, 1],[-1, 1, 1, 1, 1],[-1, 0, 0, 0, 0],[-1, 0, 0, 0, 0]])).to.eql([[1, 0, 1],[2, 0, 1]]);        
    });
    it("Peon: Primer movimiento negra", ()=>{
        expect(new Peon(6,0).vectores_movimiento([[0, 0, 0, 0, -1],[0, 0, 0, 0, -1],[1, 1, 1, 1, -1],[1, 1, 1, 1, -1],[-1, -1, -1, -1, -1]])).to.eql([[-1, 0, 1],[-2, 0, 1]]);
    });
    it("Peon: Segundo movimiento blanca", ()=>{
        let p = new Peon(1,1);
        p.x = "2";
        p.y = "0";
        p.veces_movida = 1;
        expect(p.vectores_movimiento([[-1, -1, 1, 1, 1],[-1, -1, 0, 1, 1],[-1, -1, 1, 0, 0],[-1, -1, 0, 0, 0],[-1, -1, 0, 0, 0]])).to.eql([[1, 0, 1]]);
    });
    it("Peon: Blanca puede comer una negra",()=>{
        let p = new Peon(1,1);
        p.x = "3";
        p.y = "0";
        p.veces_movida = 2;
        expect(p.vectores_movimiento([[-1, -1, 0, 1, 1],[-1, -1, 0, 0, 0],[-1, -1, 1, 0, 0],[-1, -1, 0, 2, 0],[-1, -1, 0, 0, 0]])).to.eql([[1, 0, 1],[1, 1, 1]]);
    });
    it("Peon: Negra puede comer una blanca",()=>{
        let p = new Peon(1,0);
        p.x = "5";
        p.y = "2";
        p.veces_movida = 1;
        expect(p.vectores_movimiento([[0, 0, 0, 0, 0],[0, 2, 0, 0, 0],[0, 0, 1, 0, 0],[1, 0, 0, 1, 1],[1, 1, 1, 1, 1]])).to.eql([[-1, 0, 1],[-1, -1, 1]]);
    });
    it("Peon: Blanca puede comer dos negras", ()=>{
        let p = new Peon(1,1);
        p.x = "5";
        p.y = "4";
        p.veces_movida = 4;
        expect(p.vectores_movimiento([[0, 0, 0, 0, 0],[0, 0, 0, 0, 2],[0, 0, 1, 2, 0],[0, 2, 2, 2, 0],[2, 2, 2, 2, 0]])).to.eql([[1, -1, 1],[1, 1, 1]]);
    });
    it("Peon: Blanca tiene una pieza delante en el primer movimiento", ()=>{
        expect(new Peon(2,1).vectores_movimiento([[-1, -1, -1, -1, -1],[-1, 1, 1, 1, 1],[-1, 0, 1, 1, 1],[-1, 0, 2, 0, 0],[-1, 0, 0, 0, 0]])).to.eql([]);
    });
    it("Peon: Negra tiene una pieza delante en el primer movimiento", ()=>{
        expect(new Peon(4,0).vectores_movimiento([[0, 0, 0, 0, 1],[0, 0, 2, 0, 0],[0, 1, 1, 1, 0],[1, 1, 1, 1, 1],[-1, -1, -1, -1, -1]])).to.eql([]);
    });
});

