/*
 *  By: Agufi -> GitLab: @agus.mabla 
 */

Array.prototype.equals = function (array) {    
    if (!array)
        return false;
    if (this.length != array.length)
        return false;
    for (var i = 0, l=this.length; i < l; i++) {
        if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

class Pieza{
    constructor(x, y, color){ //color: 1 -> blanco, 0 -> negro
        this.x = x;
        this.y = y;
        this.color = color; 
        this.veces_movida=0;       
    }
}
class Peon extends Pieza{
    constructor(y,color){
        if(color==1){
            super(1,y,color);
            this.simbolo = "&#9817;";
        }else{
            super(6,y,color);
            this.simbolo = "&#9823;";
        }        
    }
    vectores_movimiento(adyacentes){
        let vectores = [];        
        if(adyacentes[(this.color==1?3:1)][2] == 0){
            vectores.push([(this.color==1?1:-1),0,1]);
        }
        if(this.veces_movida == 0 && adyacentes[(this.color==1?4:0)][2] == 0){
            vectores.push([(this.color==1?2:-2),0,1]); //Direccion en x, direccion en y, cantidad repeticiones posibles.
        }
        if(adyacentes[(this.color==1?3:1)][1]==2){
            vectores.push([(this.color==1?1:-1),-1,1]);
        }
        if(adyacentes[(this.color==1?3:1)][3]==2){
            vectores.push([(this.color==1?1:-1),1,1]);
        }
        return vectores;
    }
}
class Torre extends Pieza{
    constructor(y,color){
        if(color==1){
            super(0,y,color);
            this.simbolo = "&#9814;";
        }else{
            super(7,y,color);
            this.simbolo = "&#9820;";
        }
    }
    vectores_movimiento(adyacentes){
        return [[-1,0,-1],[1,0,-1],[0,-1,-1],[0,1,-1]];
    }
}
class Caballo extends Pieza{
    constructor(y,color){
        if(color==1){
            super(0,y,color);
            this.simbolo = "&#9816;";
        }else{
            super(7,y,color);
            this.simbolo = "&#9822;";
        }
    }
    vectores_movimiento(adyacentes){
        return [[2,-1,1],[2,1,1],[1,-2,1],[1,2,1],[-1,-2,1],[-1,2,1],[-2,-1,1],[-2,1,1]];
    }
}
class Alfil extends Pieza{
    constructor(y,color){
        if(color==1){
            super(0,y,color);
            this.simbolo = "&#9815;";
        }else{
            super(7,y,color);
            this.simbolo = "&#9821;";
        }
    }
    vectores_movimiento(adyacentes){
        return [[1,-1,-1],[1,1,-1],[-1,-1,-1],[-1,1,-1]];
    }
}
class Rey extends Pieza{
    constructor(color){
        if(color==1){
            super(0,4,color);
            this.simbolo = "&#9812;";
        }else{
            super(7,4,color);
            this.simbolo = "&#9818;";
        }
    }
    vectores_movimiento(adyacentes){
        return [[1,-1,1],[1,1,1],[-1,-1,1],[-1,1,1],[1,0,1],[0,-1,1],[0,1,1],[-1,0,1]]
    }
}
class Dama extends Pieza{
    constructor(color){
        if(color==1){
            super(0,3,color);
            this.simbolo = "&#9813;";
        }else{
            super(7,3,color);
            this.simbolo = "&#9819;";
        }
    }
    vectores_movimiento(adyacentes){
        return [[1,-1,-1],[1,1,-1],[-1,-1,-1],[-1,1,-1],[1,0,-1],[0,-1,-1],[0,1,-1],[-1,0,-1]]
    }
}
class Tablero{
    constructor(id_div_tabla,id_div_msj){
        this.turno_blanco = true; //True -> le toca al blcanco, False -> le toca al negro
        this.id_div_tabla = id_div_tabla;
        this.id_div_msj = id_div_msj;
        this.tablero = [];
        this.seleccionada = null;
        this.posibles_movimientos_seleccion = null;
        for(let i=0;i<8;i++){
            let color = i<3;
            if(i==0||i==7){
                this.tablero.push([new Torre(0,color),new Caballo(1,color),new Alfil(2,color),new Dama(color),new Rey(color),new Alfil(5,color),new Caballo(6,color),new Torre(7,color)]);
            }else if(i==1||i==6){
                let temp = [];
                for(let w=0;w<8;w++){
                    temp.push(new Peon(w,color));
                }
                this.tablero.push(temp);
            }else{
                let temp = [];
                for(let w=0; w<8;w++){
                    temp.push(null);
                }
                this.tablero.push(temp);
            }
        }
        this.dibujar([]);        
    }
    dibujar(seleccion){
        let temp_tablero = "<table class='"+this.id_div_tabla+"'>";
        for(let f=7;f>=0;f--){
            temp_tablero += "<tr>";
            for(let c=0;c<8;c++){
                temp_tablero += "<td id='"+this.id_div_tabla+"_"+f+"_"+c+"' class='celda "+((f+c)%2?" c_blanca ":" c_negra ")+(this.tablero[f][c]!=null?(this.tablero[f][c].color?" p_blanca ":" p_negra "):"")+"'>" + (this.tablero[f][c] != null?this.tablero[f][c].simbolo:"&nbsp") + "</td>";
            }
            temp_tablero += "</tr>";
        }
        temp_tablero += "</table>";
        $("#"+this.id_div_tabla).html(temp_tablero)
        seleccion.forEach(selec =>{
            $("#"+this.id_div_tabla+"_"+selec[0]+"_"+selec[1]).addClass("selected");
        });
        bindEvents(this);
    }
    seleccion_pieza(x,y){
        let temp = false;
        $("#"+this.id_div_msj).html("");
        if(this.seleccionada != null){
            this.posibles_movimientos_seleccion.forEach(posible_movimiento => {
                if(posible_movimiento.equals([x,y])){
                    temp = true;
                }
            });
        }
        if(temp){
            if(this.jaque([x,y],this.seleccionada,true)){
                $("#"+this.id_div_msj).html("No puedes poner a tu propio rey en jaque")
            }else{
                if(this.jaque([x,y],this.seleccionada)){
                    $("#"+this.id_div_msj).html("Jaque!")                    
                }
                this.tablero[x][y] = this.tablero[this.seleccionada[0]][this.seleccionada[1]];
                this.tablero[this.seleccionada[0]][this.seleccionada[1]] = null;
                this.tablero[x][y].veces_movida++;
                this.tablero[x][y].x = x;
                this.tablero[x][y].y = y;
                this.turno_blanco = !this.turno_blanco;
                this.seleccionada = null;
                this.dibujar([])
                if(this.mate(this.turno_blanco)){
                    $("#"+this.id_div_msj).html("Jaque Mate!")
                }
            }
        }else if(this.tablero[x][y] != null){
            if(this.tablero[x][y].color == this.turno_blanco){
                this.seleccionada = [x,y]
                this.posibles_movimientos_seleccion = this.posibles_movimientos(x,y,this.tablero);
                this.dibujar(this.posibles_movimientos_seleccion);
            }
        }
    }

    posibles_movimientos(a,b,tablero){
        let x = parseInt(a);
        let y = parseInt(b);
        let adyacentes = [[],[],[],[],[]]
        if(x<2||x>5){
            adyacentes[(x<2?0:4)] = [-1,-1,-1,-1,-1];
        }
        if(x==0 || x==7){
            adyacentes[(x==0?1:3)] = [-1,-1,-1,-1,-1];
        }
        if(y<2||y>5){
            for(let i=0;i<5;i++){
                adyacentes[i][(y<2?0:4)] = -1;
            }
        }
        if(y==0 || y==7){
            for(let i=0;i<5;i++){
                adyacentes[i][(y==0?1:3)] = -1;
            }
        }
        for(let i=-2;i<3;i++){
            for(let w=-2;w<3;w++){
                if(tablero[x+i] !== undefined){
                    if(tablero[x+i][y+w] !== undefined){
                        adyacentes[i+2][w+2] = (tablero[x+i][y+w]==null?0:(tablero[x+i][y+w].color == tablero[x][y].color?1:2)); //Devuelve: 0->casilla libre, 1-> casilla ocupada por una pieza del mismo color, 2-> casilla ocupada por una pieza del color opuesto
                    }
                }
            }
        }
        let vectores = tablero[x][y].vectores_movimiento(adyacentes);
        let posiciones = []
        vectores.forEach(vector => {
            let posicion_actual_imaginaria = [x,y]
            let cont = 0;
            while(cont != vector[2]){
                posicion_actual_imaginaria  = [posicion_actual_imaginaria[0]+vector[0],posicion_actual_imaginaria[1]+vector[1]];
                if(posicion_actual_imaginaria[0]<0||posicion_actual_imaginaria[0]>7||posicion_actual_imaginaria[1]<0||posicion_actual_imaginaria[1]>7){
                    break;
                }
                if(tablero[posicion_actual_imaginaria[0]][posicion_actual_imaginaria[1]]==null){
                    posiciones.push(posicion_actual_imaginaria);
                }else if(tablero[posicion_actual_imaginaria[0]][posicion_actual_imaginaria[1]].color != tablero[x][y].color){
                    posiciones.push(posicion_actual_imaginaria);
                    break;
                }else{
                    break;
                }            
                cont ++;
            }
        });
        return(posiciones);
    }

    
    jaque(mover_a,seleccionada,riesgo=false){
        let temp = [];
        this.tablero.forEach(f => {
            temp.push(f.slice());
        });
        temp[mover_a[0]][mover_a[1]] = temp[seleccionada[0]][seleccionada[1]];
        temp[seleccionada[0]][seleccionada[1]] = null;
        temp[mover_a[0]][mover_a[1]].x=mover_a[0];
        temp[mover_a[0]][mover_a[1]].y=mover_a[1];
        temp[mover_a[0]][mover_a[1]].veces_movida++;
        let riesgo_al_rey = false;
        let rey = null;
        temp.forEach(f =>{
            f.forEach(x =>{
                if(x != null){
                    if(x.constructor.name =="Rey" && x.color == (riesgo?this.turno_blanco:!this.turno_blanco)){
                        rey = x;
                    }
                }
            });            
        });
        temp.forEach(w=>{
            w.forEach(casilla => {
                if(casilla != null){
                    if(casilla.color == (riesgo?!this.turno_blanco:this.turno_blanco)){
                        this.posibles_movimientos(casilla.x,casilla.y,temp).forEach(p =>{
                            if(p[0]==rey.x && p[1]==rey.y){
                                riesgo_al_rey = true;                        
                            }
                        });
                    }
                }
            });
        });
        temp[mover_a[0]][mover_a[1]].x=seleccionada[0];
        temp[mover_a[0]][mover_a[1]].y=seleccionada[1];
        temp[mover_a[0]][mover_a[1]].veces_movida--;
        return riesgo_al_rey;
    }
    mate(mate_para_blancas){
        let mate = true;
        this.tablero.forEach(f=>{
            f.forEach(c=>{
                if(c != null){
                    if(c.color == mate_para_blancas){
                        this.posibles_movimientos(c.x,c.y,this.tablero).forEach(m=>{
                            if(!this.jaque(m,[c.x,c.y],true)){
                                mate = false;
                            }
                        });
                    }

                }
            });            
        });
        return mate;
    }

}


function bindEvents(tablero){
    $("."+tablero.id_div_tabla+" > tbody >tr >.celda").on("click", function(){
        let self_id = $(this).attr('id').split("_");
        tablero.seleccion_pieza(self_id[1],self_id[2]);
    });
    let max =0;
    $("."+tablero.id_div_tabla+" > tbody >tr >.celda").each(function(){
        if($(this).width() > max){
            max = $(this).width();
        }
        if($(this).height() > max){
            max = $(this).height();
        }
    });
    $(".celda").width(max);
    $(".celda").height(max);
}

$(document).ready(function(){
    t1 = new Tablero("tablero","msj");
});